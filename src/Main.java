import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int dimension;
        int[] initialBoard;
        PuzzleNode initialNode;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the dimensions of your board.");
        dimension=sc.nextInt();
        int boardLength=dimension*dimension;
        initialBoard=new int[boardLength];
        System.out.println("Enter the tile values one by one. Use dimension squared to indicate the empty tile.");
        for(int i=0; i<boardLength; i++)
        {
            initialBoard[i]=sc.nextInt();
        }

        initialNode=PuzzleNode.getInitialNode(initialBoard, dimension);

        //Search.hammingPrioritySearch(initialNode);
        //Search.manhattanPrioritySearch(initialNode);
        Search.linearConflictPrioritySearch(initialNode);

    }
}

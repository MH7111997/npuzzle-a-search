import java.util.ArrayList;
import java.util.Arrays;

public class PuzzleNode {
    private static int BOARD_DIMENSION;
    private static int EMPTY_VALUE;

    public int[] board;
    public int pathCost;
    public PuzzleNode parent;
    public int emptyIndex;

    private PuzzleNode(PuzzleNode parent, int[] board, int emptyIndex)
    {
        this.board=board.clone();
        this.pathCost=parent.pathCost+1;
        this.parent=parent;
        this.emptyIndex=emptyIndex;
    }

    private PuzzleNode(int[] board)
    {
        this.board=board.clone();
    }

    public static PuzzleNode getInitialNode(int[] board, int dimension)
    {
        /*initialization*/
        BOARD_DIMENSION=dimension;
        EMPTY_VALUE=board.length;

        PuzzleNode start= new PuzzleNode(board);
        start.pathCost=0;
        start.parent=null;
        for(int i=0; i<EMPTY_VALUE; i++)
        {
            if(start.board[i]==EMPTY_VALUE)
            {
                start.emptyIndex=i;
                break;
            }
        }
        return start;
    }

    public boolean isGoalState()
    {
        for (int i=0; i<EMPTY_VALUE; i++)
        {
            if(this.board[i]!=i+1) return false;
        }

        return true;
    }

    public ArrayList<PuzzleNode> getAllSuccessors()
    {
        int[] temp =this.board.clone();
        ArrayList<PuzzleNode> successors = new ArrayList<>();
        PuzzleNode successor;

        /*left*/
        if(((this.emptyIndex)%BOARD_DIMENSION)!=0)
        {
            int swapValue=this.board[emptyIndex-1];
            temp[emptyIndex]=swapValue;
            temp[emptyIndex-1]=EMPTY_VALUE;

            successor=new PuzzleNode(this, temp, this.emptyIndex-1);
            successors.add(successor);
        }

        temp=this.board.clone();

        /*right*/
        if(((this.emptyIndex)%BOARD_DIMENSION)!=BOARD_DIMENSION-1)
        {
            int swapValue=this.board[emptyIndex+1];
            temp[emptyIndex]=swapValue;
            temp[emptyIndex+1]=EMPTY_VALUE;

            successor=new PuzzleNode(this, temp, this.emptyIndex+1);
            successors.add(successor);
        }

        temp=this.board.clone();

        /*up*/
        if(((this.emptyIndex)/BOARD_DIMENSION)!=0)
        {
            int swapValue=this.board[emptyIndex-BOARD_DIMENSION];
            temp[emptyIndex]=swapValue;
            temp[emptyIndex-BOARD_DIMENSION]=EMPTY_VALUE;

            successor=new PuzzleNode(this, temp, this.emptyIndex-BOARD_DIMENSION);
            successors.add(successor);
        }

        temp=this.board.clone();

        /*down*/
        if(((this.emptyIndex)/BOARD_DIMENSION)!=BOARD_DIMENSION-1)
        {
            int swapValue=this.board[emptyIndex+BOARD_DIMENSION];
            temp[emptyIndex]=swapValue;
            temp[emptyIndex+BOARD_DIMENSION]=EMPTY_VALUE;

            successor=new PuzzleNode(this, temp, this.emptyIndex+BOARD_DIMENSION);
            successors.add(successor);
        }

        return successors;
    }

    @Override
    public boolean equals(Object obj) {
        PuzzleNode temp =(PuzzleNode)obj;
        for (int i=0; i<EMPTY_VALUE; i++)
        {
            if(this.board[i]!=temp.board[i]) return false;
        }
        return true;
    }

    private int countInversions()
    {
        int inversionCount=0;
        for(int i=0; i<EMPTY_VALUE; i++)
        {
            for(int j=i+1; j<EMPTY_VALUE; j++)
            {
                if(board[i]!=EMPTY_VALUE)
                {
                    if(board[i]>board[j]) inversionCount++;

                }
            }
        }

        return inversionCount;
    }

    public boolean isSolvable()
    {
        int inversionCount=countInversions();
        if((BOARD_DIMENSION%2)!=0)
        {
            return ((inversionCount%2)==0);
        }
        else
        {
            return ((inversionCount+(emptyIndex/BOARD_DIMENSION))%2==1);
        }
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int i=0; i<EMPTY_VALUE; i++)
        {
            if(this.board[i]!=EMPTY_VALUE)
            {
                string.append(this.board[i]);
            }
            else string.append(" ");

            string.append(" ");
            if((i%BOARD_DIMENSION)==(BOARD_DIMENSION-1))
            {
                string.append("\n");
            }
        }

        string.append("\n");
        return string.toString();
    }

    public int printState()
    {
        int numberOfStates=0;
        if(this.parent!=null)
        {
            numberOfStates=this.parent.printState();
        }

        numberOfStates++;
        System.out.println(this.toString());
        return numberOfStates;
    }

    public int getHammingDistanceFromGoal()
    {
        int distance=0;
        for(int i=0; i< EMPTY_VALUE; i++)
        {
            if(board[i]!=EMPTY_VALUE)
            {
                if(board[i]!=i+1)
                {
                    distance++;
                }
            }
        }

        return distance;
    }

    public int getManhattanDistanceFromGoal()
    {
        int distance=0;
        for(int i=0; i<EMPTY_VALUE; i++)
        {
            if(board[i]!=EMPTY_VALUE)
            {
                int correctRow=(board[i]-1)/BOARD_DIMENSION;
                int correctColumn=(board[i]-1)%BOARD_DIMENSION;

                int distanceOfThisTile=Math.abs(correctRow-(i/BOARD_DIMENSION))+Math.abs(correctColumn-(i%BOARD_DIMENSION));
                distance+=distanceOfThisTile;
            }
        }
        return distance;
    }

    private int[] getRow(int number)
    {
        int[] row = new int[BOARD_DIMENSION];
        for(int i=0; i<BOARD_DIMENSION; i++)
        {
            row[i]=this.board[number*BOARD_DIMENSION+i];
        }

        return row;
    }

    private int[] getColumn(int number)
    {
        int[] column = new int[BOARD_DIMENSION];
        for (int i=0; i<BOARD_DIMENSION; i++)
        {
            column[i]=this.board[BOARD_DIMENSION*i+number];
        }
        return column;
    }

    private int getRowOfNumber(int number)
    {
        return (number-1)/BOARD_DIMENSION;
    }

    private int getColumnOfNumber(int number)
    {
        return  (number-1)%BOARD_DIMENSION;
    }

    private int countRowLinearConflicts()
    {
        int rowLinearConflicts=0;
        for(int i=0; i<BOARD_DIMENSION; i++)
        {
            int[] row=getRow(i);
            for(int j=0; j<BOARD_DIMENSION; j++)
            {
                for(int k=j+1; k<BOARD_DIMENSION; k++)
                {
                    if((row[j]!=EMPTY_VALUE)&&(row[k]!=EMPTY_VALUE))
                    {
                        if(((getRowOfNumber(row[j]))==i)&&((getRowOfNumber(row[k]))==i))
                        {
                            if(row[j]>row[k]) rowLinearConflicts++;
                        }
                    }
                }
            }
        }

        return rowLinearConflicts;
    }

    private int countColumnLinearConflicts()
    {
        int columnLinearConflicts=0;
        for(int i=0; i<BOARD_DIMENSION; i++)
        {
            int[] column=getColumn(i);
            for (int j=0; j<BOARD_DIMENSION; j++)
            {
                for(int k=j+1; k<BOARD_DIMENSION; k++)
                {
                    if(column[j]!=EMPTY_VALUE&&column[k]!=EMPTY_VALUE)
                    {
                        if(getColumnOfNumber(column[j])==i&&getColumnOfNumber(column[k])==i)
                        {
                            if(column[j]>column[k]) columnLinearConflicts++;
                        }
                    }
                }
            }
        }
        return columnLinearConflicts;
    }

    public int countLinearConflicts()
    {
        return this.countRowLinearConflicts()+this.countColumnLinearConflicts();
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.board);
    }
}

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;

public class Search {
    private static void genericUniformCostSearch(PuzzleNode start, Comparator<PuzzleNode> comparator)
    {
        int numberOfNodesExpanded=0;
        int numberOfNodesExplored=0;
        if(!start.isSolvable())
        {
            System.out.println("This instance can not be solved.");
            return;
        }
        PriorityQueue<PuzzleNode> frontier = new PriorityQueue<>(comparator);
        HashSet<PuzzleNode> closedSet = new HashSet<>();
        frontier.add(start);
        while (!frontier.isEmpty())
        {
            PuzzleNode current=frontier.poll();
            numberOfNodesExpanded++;
            if(current.isGoalState())
            {
                int steps=current.printState();
                steps--;
                System.out.println("Solved!");
                System.out.println("Steps: "+steps);
                System.out.println("Number of nodes expanded: " +numberOfNodesExpanded);
                System.out.println("Number of nodes explored: "+numberOfNodesExplored);
                return;
            }
            closedSet.add(current);
            ArrayList<PuzzleNode> successors=current.getAllSuccessors();
            for (PuzzleNode p: successors
                 ) {
                numberOfNodesExplored++;
                if(!closedSet.contains(p))
                {
                    frontier.add(p);
                }
            }
        }

    }

    public static void hammingPrioritySearch(PuzzleNode start)
    {
        genericUniformCostSearch(start, new Comparator<PuzzleNode>(){
            @Override
            public int compare(PuzzleNode o1, PuzzleNode o2) {
                 return (o1.pathCost+o1.getHammingDistanceFromGoal())-(o2.pathCost+o2.getHammingDistanceFromGoal());
            }
        });
    }

    public static void manhattanPrioritySearch(PuzzleNode start)
    {
        genericUniformCostSearch(start, new Comparator<PuzzleNode>() {
            @Override
            public int compare(PuzzleNode o1, PuzzleNode o2) {
                return (o1.pathCost+o1.getManhattanDistanceFromGoal())-(o2.pathCost+o2.getManhattanDistanceFromGoal());
            }
        });
    }

    public static void linearConflictPrioritySearch(PuzzleNode start)
    {
        genericUniformCostSearch(start, new Comparator<PuzzleNode>() {
            @Override
            public int compare(PuzzleNode o1, PuzzleNode o2) {
                return (o1.pathCost+o1.getManhattanDistanceFromGoal()+2*o1.countLinearConflicts())-(o2.pathCost+o2.getManhattanDistanceFromGoal()+2*o2.countLinearConflicts());
            }
        });
    }
}
